# Notes

Still testing, but something like this...

Move this script to:

	sudo install -m755 -o root -g wheel postgres.server /opt/local/etc/LaunchDaemons/org.macports.postgresql95-server

Backup the MacPort wrapper:

	cd /opt/local/etc/LaunchDaemons/org.macports.postgresql95-server
	mv postgresql95-server.wrapper{,.orig}


Make a symlink...

	$ sudo ln -s postgres{.server,ql95-server.wrapper}

Add to `~/.bash_aliases` _(or where you keep your aliases)_ so you can use it directly.

	alias postgres.server='/opt/local/etc/LaunchDaemons/org.macports.postgresql95-server/postgres.server'

...or move it into a location within PATH and make the _symlink_ from there.


### Usage

As a drop-in replacement, to the original wrapper, you can use it as normal with `sudo port (load|unload|restart) postgresql95-server`, for those commands _(start/stop/restart)_.

With the alias you can run the xtras separately, like:

- `postgres.server initdb`
- `postgres.server reload`
- `postgres.server status`


You should run the MacPorts commands to use the daemon, but you could use it the same way…

- `postgres.server start`
- `postgres.server stop`
- `postgres.server restart`